
import psycopg2


def add_salaries(conn, ids, employee_ids, amount):
    cursor = conn.cursor()

    for i in range(0, len(ids)):
        try:
            cursor.execute("INSERT INTO salaries VALUES (%s, %s, %s);",
                           (ids[i], employee_ids[i], amount[i]))
        except Exception:
            print("Error: empty id or duplicate id/employee_id")
            conn.rollback()
            cursor.close()
    conn.commit()
    cursor.close()


connection = psycopg2.connect(
    dbname='lab9',
    user='username',
    password='postgres',
    host='localhost',
)

add_salaries(connection, [1, 2], [1, 2], [1, 2])
add_salaries(connection, [1], [1], [1000])
add_salaries(connection, [None], [1], [1000])
